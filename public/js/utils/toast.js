$("#toast-success").on("hidden.bs.toast", function () {
    $("#toast-success-title").text("Informasi");
    $("#toast-success-message").text("");
    $("#toast-success").hide();
});

$("#toast-warning").on("hidden.bs.toast", function () {
    $("#toast-warning-title").text("Perhatian");
    $("#toast-warning-message").text("");
    $("#toast-warning").hide();
});

$("#toast-danger").on("hidden.bs.toast", function () {
    $("#toast-danger-title").text("Error");
    $("#toast-danger-message").text("");
    $("#toast-danger").hide();
});

function showSuccessToast(message) {
    $("#toast-success").show();

    $("#toast-success").toast("hide");
    $("#toast-warning").toast("hide");
    $("#toast-danger").toast("hide");

    setTimeout(() => {
        $("#toast-success-title").text("Informasi");
        $("#toast-success-message").text(message);
        $("#toast-success").toast("show");
    }, 500);
}

function showWarningToast(message) {
    $("#toast-warning").show();

    $("#toast-warning").toast("hide");
    $("#toast-danger").toast("hide");
    $("#toast-success").toast("hide");

    setTimeout(() => {
        $("#toast-warning-title").text("Perhatian");
        $("#toast-warning-message").text(message);
        $("#toast-warning").toast("show");
    }, 500);
}

function showDangerToast(message) {
    $("#toast-danger").show();

    $("#toast-success").toast("hide");
    $("#toast-warning").toast("hide");
    $("#toast-danger").toast("hide");

    setTimeout(() => {
        $("#toast-danger-title").text("Error");
        $("#toast-danger-message").text(message);
        $("#toast-danger").toast("show");
    }, 500);
}
