function showLoading() {
    $("#modal-loading").modal({
        backdrop: "static",
        keyboard: false,
        show: true,
    });
}

function hideLoading() {
    $("#modal-loading").modal("hide");
}
