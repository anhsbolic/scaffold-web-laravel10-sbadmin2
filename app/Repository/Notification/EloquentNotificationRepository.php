<?php

namespace App\Repository\Notification;

use App\Models\Notification;

class EloquentNotificationRepository implements NotificationRepositoryInterface
{
    public function userNotifications(string $userId)
    {
        return Notification::where('to_user_id', $userId)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function latestUserNotifications(string $userId)
    {
        return Notification::where('to_user_id', $userId)
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();
    }

    public function totalUserNotifications(string $userId)
    {
        return Notification::where('to_user_id', $userId)->count();
    }

    public function totalUnreadUserNotifications(string $userId)
    {
        return Notification::where([
            'to_user_id' => $userId,
            'read_flag' => Notification::FLAG_UNREAD
        ])->count();
    }

    public function userNotificationById(int $id, string $userId)
    {
        return Notification::where([
            'id' => $id,
            'to_user_id' => $userId
        ])->first();
    }

    public function create(Notification $notification)
    {
        $notification->save();
    }

    public function update(Notification $notification)
    {
        $notification->save();
    }

    public function delete(Notification $notification)
    {
        $notification->delete();
    }

    public function readAll(string $userId)
    {
        $notifications = Notification::where([
            'to_user_id' => $userId,
            'read_flag' => Notification::FLAG_UNREAD
        ])->get();

        foreach ($notifications as $notification) {
            $notification->read_flag = Notification::FLAG_READ;
            $notification->save();
        }
    }
}
