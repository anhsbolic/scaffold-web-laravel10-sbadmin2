<?php

namespace App\Repository\Notification;

use App\Models\Notification;

interface NotificationRepositoryInterface
{
    public function userNotifications(string $userId);
    public function latestUserNotifications(string $userId);
    public function totalUserNotifications(string $userId);
    public function totalUnreadUserNotifications(string $userId);
    public function userNotificationById(int $id, string $userId);

    public function create(Notification $notification);
    public function update(Notification $notification);
    public function delete(Notification $notification);
    public function readAll(string $userId);
}
