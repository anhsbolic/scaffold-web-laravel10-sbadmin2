<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    const FLAG_UNREAD = 0;
    const FLAG_READ = 1;
    const TYPE_GENERAL = 1;
}
