<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class HostHeaderProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // whitelist
        $whiteListHost = [env('HOST_PROD'), env('HOST_PROD_WWW')];
        if (App::environment('local')) {
            array_push($whiteListHost, env('HOST_LOCAL'));
        }
        // get host name
        $host = "";
        $header = $request->header();
        $headerHost = $header['host'];
        if (!$headerHost) {
            Log::warning("Forbidden Host: " . $host);
            abort(403, "Forbidden");
        }
        if (count($headerHost) <= 0) {
            Log::warning("Forbidden Host: " . $host);
            abort(403, "Forbidden");
        }
        $host = $headerHost[0];

        // host validation
        if (!in_array($host, $whiteListHost)) {
            $message = "Not Allowed Host " . $host;
            $user_agent = $request->header('User-Agent');
            if ($user_agent) {
                $message = $message . ". With user agent " . $user_agent;
            }
            Log::warning($message);
            abort(403, "Forbidden");
        }
        // next
        return $next($request);
    }
}
