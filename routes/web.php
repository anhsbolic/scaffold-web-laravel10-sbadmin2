<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['revalidateBackHistory', 'hostHeaderProtection']], function () {
    /*
    |--------------------------------------------------------------------------
    | PUBLIC ROUTES
    |--------------------------------------------------------------------------
    */
    Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
    Route::get('/login', function () {
        return redirect('/');
    });
    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('postLogin');

    /*
    |--------------------------------------------------------------------------
    | PRIVATE ROUTES
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => ['customWebAuth']], function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
    });
});
