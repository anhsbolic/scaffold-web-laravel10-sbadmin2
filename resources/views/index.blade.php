@extends('layouts.beforeLogin')

@section('pageTitle')
    <title>Checksheets Management</title>
@endsection

@section('localStyle')
    <style>
        .form-control-user {
            font-size: 1rem;
            border-radius: 4px !important;
            padding: 1.5rem 1rem;
        }

        .btn-user {
            font-size: 1rem !important;
            border-radius: 8px !important;
            padding: .75rem 1rem;
        }

        .form-control {
            font-size: 1rem !important;
        }
    </style>
@endsection

@section('content')
    <div class="container" style="min-height: 100vh">
        <div class="row justify-content-center" style="min-height: 100vh">
            <div class="col-xl-8 col-lg-8 col-md-8 my-auto">
                <div class="card o-hidden border-0 shadow-lg my-5" style="min-height: 100%">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-5 d-none d-lg-block mx-auto text-center">
                                <img class="mx-auto text-center" style="width: 60%; height: 100%; object-fit: contain"
                                    src="{{ asset('img/logo-app.png') }}" />
                            </div>
                            <div class="col-lg-7 my-auto">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back</h1>
                                    </div>

                                    @if (Session::has('success'))
                                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                                    @endif

                                    @if (Session::has('danger'))
                                        <div class="alert alert-danger">{{ Session::get('danger') }}</div>
                                    @endif

                                    <form class="user" role="form" method="POST" action="{{ route('postLogin') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input id="input-email" name="email" type="email"
                                                class="form-control form-control-user" placeholder="Email" required>
                                        </div>
                                        <div class="form-group">
                                            <input id="input-password" name="password" type="password"
                                                class="form-control form-control-user" placeholder="Password" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block mt-5">
                                            Login
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection
