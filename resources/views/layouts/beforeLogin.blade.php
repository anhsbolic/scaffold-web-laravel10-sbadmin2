<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    @yield('pageTitle')
    @include('includes.beforeLogin.style')
    @yield('localStyle')
</head>

<body class="bg-gradient-primary" style="min-height: 100vh">
    @yield('content')

    {{-- LOADING SPINNER MODAL --}}
    @include('includes.modalLoading')

    {{-- INCLUDE SCRIPTS --}}
    @include('includes.beforeLogin.script')
</body>

</html>
