@extends('layouts.afterLogin')

@section('pageTitle')
    <title>Home</title>
@endsection

@section('localStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <h5 class="pt-4 pb-1 text-gray-800">HOME</h5>
    </div>
@endsection

@section('js')
@endsection
