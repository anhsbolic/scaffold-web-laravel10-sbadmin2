<div id="toast-success" class="toast" data-autohide="true" data-delay="3000"
    style="display: none; position: absolute; top: 5rem; right: 1.25rem; z-index: 100; min-width: 240px;">
    <div class="toast-header" style="background-color: #1cc88a !important;">
        <b id="toast-success-title" class="mr-auto" style="color: white"></b>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body" style="background-color: #9cdfc71f !important;">
        <p id="toast-success-message" class="mb-0 pb-0" style="font-size: 14px; font-weight: 500;"></p>
    </div>
</div>

<div id="toast-warning" class="toast" data-autohide="true" data-delay="3000"
    style="display: none; position: absolute; top: 5rem; right: 1.25rem; z-index: 100; min-width: 240px;">
    <div class="toast-header" style="background-color: #f6c23e !important;">
        <b id="toast-warning-title" class="mr-auto" style="color: white"></b>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body" style="background-color: #f6c23e17 !important;">
        <p id="toast-warning-message" class="mb-0 pb-0" style="font-size: 14px; font-weight: 500;"></p>
    </div>
</div>

<div id="toast-danger" class="toast" data-autohide="true" data-delay="3000"
    style="display: none; position: absolute; top: 5rem; right: 1.25rem; z-index: 100; min-width: 240px;">
    <div class="toast-header" style="background-color: #e74a3b !important;">
        <b id="toast-danger-title" class="mr-auto" style="color: white"></b>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body" style="background-color: #e74a3b0d !important;">
        <p id="toast-danger-message" class="mb-0 pb-0" style="font-size: 14px; font-weight: 500;"></p>
    </div>
</div>
