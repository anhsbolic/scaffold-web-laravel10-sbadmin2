@php
    use App\Models\Notification;
    use App\Repository\Notification\EloquentNotificationRepository;

    // USER
    $avatar = null;
    // $avatarFilename = $miminProfile->avatar;
    // if ($avatarFilename != null && $avatarFilename != "") {
    // $avatar = $assetHelper->miminAvatarBase64($avatarFilename);
    // }

    // NOTIFICATION
    $notificationRepository = new EloquentNotificationRepository();
    // TODO : uncomment this
    // $notifTotal = $notificationRepository->totalUserNotifications(Auth::id());
    // $unreadNotifTotal = $notificationRepository->totalUnreadUserNotifications(Auth::id());
    // $latestNotifications = $notificationRepository->latestUserNotifications(Auth::id());
    // TODO : DELETE THIS
    $notifTotal = 0;
    $unreadNotifTotal = 0;
    $latestNotifications = [];

@endphp
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" aria-haspopup="true" aria-expanded="false">
                <img height="40px" src="{{ asset('img/logo-app.png') }}">
            </a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow ml-1 mr-3">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                @if ($unreadNotifTotal > 0)
                    <span class="badge badge-danger badge-counter">
                        @if ($unreadNotifTotal >= 100)
                            99+
                        @else
                            {{ $unreadNotifTotal }}
                        @endif
                    </span>
                @endif
            </a>
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Notifikasi
                </h6>

                @if ($notifTotal > 0)
                    @foreach ($latestNotifications as $latestNotification)
                        <a class="dropdown-item d-flex align-items-center" href="{{ $latestNotification->url }}">
                            <div class="mr-3">
                                <div class="icon-circle bg-primary">
                                    <i class="fas fa-file-contract text-white"></i>
                                </div>
                            </div>
                            <div>
                                <div class="small text-gray-500">
                                    {{ $latestNotification->created_at->diffForHumans() }}
                                </div>
                                <span class="{{ $latestNotification->read_flag ? '' : 'font-weight-bold' }}">
                                    {{ $latestNotification->content }}
                                </span>
                            </div>
                        </a>
                    @endforeach
                @else
                    <div class="dropdown-item d-flex align-items-center py-3">
                        Belum Ada Notifikasi
                    </div>
                @endif

                {{-- <a class="dropdown-item text-center small text-gray-500" href="#">Lihat Semua</a> --}}
                <a class="dropdown-item text-center small text-gray-500" href="#">&nbsp;</a>
            </div>
        </li>

        <div class="topbar-divider d-none d-sm-block ml-0 mr-0"></div>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle"
                    src="{{ $avatar != null ? $avatar : asset('img/placeholder/avatar-admin.png') }}">
                <span class="ml-2 d-none d-lg-inline text-gray-600 small">Username</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="">
                    <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>
                    Ganti Password
                </a>
                <hr class="mt-2 mb-2">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
