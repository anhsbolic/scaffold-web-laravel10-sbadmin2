<script src="{{ url('template/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ url('template/js/sb-admin-2.min.js') }}"></script>
<script src="{{ url('js/utils/toast.js') }}"></script>
<script src="{{ url('js/utils/modal-loading.js') }}"></script>
<script>
    const siteUrl = '{{ url('') }}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('js')
