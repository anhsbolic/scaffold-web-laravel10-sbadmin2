@php
    $userRoleName = 'USER ROLE NAME';
    $pathSegment1 = Request::segment(1);
    $pathSegment2 = Request::segment(2);
@endphp
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-text mx-3">{{ $userRoleName }}</div>
    </a>

    {{-- MENUS --}}
    <hr class="sidebar-divider mt-2 mb-0">
    <div class="sidebar-heading mt-2 mb-2">
        Home
    </div>
    <li class="nav-item {{ $pathSegment1 == '' || $pathSegment1 == 'home' ? 'active' : '' }}">
        <a class="nav-link {{ $pathSegment1 == 'home' ? '' : 'collapsed' }} pt-0 pb-3" href="#"
            data-toggle="collapse" data-target="#collapseMenuHome" aria-expanded="true"
            aria-controls="collapseMenuHome">
            <i class="fas fa-fw fa-ticket-alt"></i>
            <span>Home</span>
        </a>
        <div id="collapseMenuHome" class="collapse  {{ $pathSegment1 == '' || $pathSegment1 == 'home' ? 'show' : '' }}"
            aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item  {{ $pathSegment1 == '' ? 'active' : '' }}" href="">
                    Submenu 1
                </a>
                <a class="collapse-item  {{ $pathSegment1 == 'home' && $pathSegment2 == 'submenu' ? 'active' : '' }}"
                    href="">
                    Submenu 2
                </a>
            </div>
        </div>
    </li>

    {{-- TOGGLE --}}
    <hr class="sidebar-divider mt-2">
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
